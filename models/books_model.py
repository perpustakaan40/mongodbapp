from pymongo import MongoClient
from bson import ObjectId


class database:
    def __init__(self):
        try:
            self.nosql_db = MongoClient()
            self.db = self.nosql_db.perpustakaan
            self.mongo_col = self.db.books
            print("database connected")
        except Exception as e:
            print(e)

    def showBooks(self):
        try:
            books = [b for b in self.mongo_col.find()]
            return books
        except Exception as e:
            print(e)

    def showBookById(self, params):
        try:
            query = {"_id": ObjectId(params["id"])}
            result = self.mongo_col.find_one(query)
            return result
        except Exception as e:
            print(e)

    def showBookByName(self, key):
        try:
            query = {"nama": {"$regex": key, "$options": "i"}}
            result = self.mongo_col.find(query)
            return result
        except Exception as e:
            print(e)

    def insertBook(self, document):
        try:
            self.mongo_col.insert_one(document)
        except Exception as e:
            print(e)

    def updateBookById(self, params):
        try:
            query1 = {"_id": ObjectId(params["id"])}
            query2 = {"$set": params["data"]}
            self.mongo_col.update_one(query1, query2)
        except Exception as e:
            print(e)

    def deleteBookById(self, params):
        try:
            query = {"_id": ObjectId(params["id"])}
            self.mongo_col.delete_one(query)
        except Exception as e:
            print(e)
