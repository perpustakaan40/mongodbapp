from pymongo import MongoClient
from models.books_model import database as db
import csv
import json
from bson import ObjectId


def objIdToStr(obj):
    return str(obj["_id"])


def search_books_by_name(**params):
    result = db.showBookById(params)
    print(result)
    result['_id'] = objIdToStr(result)
    return result


def search_books():
    data_list = []
    for book in db.showBooks():
        book["_id"] = objIdToStr(book)
        data_list.append(book)
    return data_list


def search_books_id(**params):
    result = db.showBookById(params)
    print(result)
    result['_id'] = objIdToStr(result)
    return result


def ubah_data(**params):
    try:
        db.updateBookById(params)
    except Exception as e:
        print(e)


if __name__ == "__main__":
    db = db()
    print(db.showBooks())
    # params = {'id': '51df07b094c6acd67e492f41'}
    # #params = {'name.first': 'John'}
    # search_books_by_name(**params)
